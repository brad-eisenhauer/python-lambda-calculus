import unittest

from lambda_calculus.boolean import FALSE, TRUE
from lambda_calculus.numbers import (
    ADD,
    FOUR,
    ISZERO,
    MUL,
    ONE,
    PRED,
    SUB,
    THREE,
    TWO,
    ZERO,
    SUCC,
)


def num_to_int(n):
    return n(lambda x: x + 1)(0)


class NumbersTests(unittest.TestCase):
    def test_succ_zero_is_one(self):
        self.assertEqual(num_to_int(ONE), num_to_int(SUCC(ZERO)))

    def test_succ_four_is_five(self):
        self.assertEqual(5, num_to_int(SUCC(FOUR)))

    def test_add_one_and_two_is_three(self):
        self.assertEqual(num_to_int(THREE), num_to_int(ADD(ONE)(TWO)))

    def test_two_times_three_is_six(self):
        self.assertEqual(6, num_to_int(MUL(TWO)(THREE)))

    def test_pred_four_is_three(self):
        self.assertEqual(num_to_int(THREE), num_to_int(PRED(FOUR)))

    def test_four_minus_three_is_one(self):
        self.assertEqual(num_to_int(ONE), num_to_int(SUB(FOUR)(THREE)))

    def test_zero_is_zero(self):
        self.assertIs(TRUE, ISZERO(ZERO))

    def test_one_is_not_zero(self):
        self.assertIs(FALSE, ISZERO(ONE))

    def test_two_is_not_zero(self):
        self.assertIs(FALSE, ISZERO(TWO))


if __name__ == "__main__":
    unittest.main()
