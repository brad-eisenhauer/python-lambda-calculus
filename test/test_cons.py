import unittest

from lambda_calculus.boolean import FALSE, TRUE
from lambda_calculus.cons import CAR, CDR, CONS


class ConsTests(unittest.TestCase):
    def test_cons_2_3_true_returns_2(self):
        self.assertEqual(2, CONS(2)(3)(TRUE))

    def test_cons_2_3_false_returns_3(self):
        self.assertEqual(3, CONS(2)(3)(FALSE))

    def test_car_of_cons_2_3_returns_2(self):
        self.assertEqual(2, CAR(CONS(2)(3)))

    def test_cdr_of_cons_2_3_returns_3(self):
        self.assertEqual(3, CDR(CONS(2)(3)))


if __name__ == "__main__":
    unittest.main()
