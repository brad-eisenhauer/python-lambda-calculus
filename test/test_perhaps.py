import unittest

from lambda_calculus.perhaps import Perhaps


class PerhapsTests(unittest.TestCase):
    DATA = {"a": {"b": {"c": 42}}}

    def test_get_a_b_c_is_42(self):
        self.assertEqual(
            42,
            (
                Perhaps(self.DATA)
                >> (lambda d: d.get("a"))
                >> (lambda a: a.get("b"))
                >> (lambda b: b.get("c"))
            ).value,
        )

    def test_get_b_is_none(self):
        self.assertIsNone((Perhaps(self.DATA) >> (lambda d: d.get("b"))).value)


if __name__ == "__main__":
    unittest.main()
