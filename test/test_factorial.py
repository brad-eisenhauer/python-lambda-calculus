import unittest

from lambda_calculus.factorial import FACT
from lambda_calculus.numbers import ADD, FOUR, MUL, ONE, THREE, TWO, ZERO


def num_to_int(n):
    return n(lambda x: x + 1)(0)


class FactorialTests(unittest.TestCase):
    def test_zero_factorial_is_one(self):
        self.assertEqual(num_to_int(ONE), num_to_int(FACT(ZERO)))

    def test_three_factorial_is_six(self):
        self.assertEqual(num_to_int(MUL(TWO)(THREE)), num_to_int(FACT(THREE)))

    def test_four_factorial_is_twenty_four(self):
        twenty_four = ADD(MUL(THREE)(FOUR))(MUL(THREE)(FOUR))
        self.assertEqual(num_to_int(twenty_four), num_to_int(FACT(FOUR)))


if __name__ == "__main__":
    unittest.main()
