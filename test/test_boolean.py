import unittest

from lambda_calculus.boolean import AND, FALSE, NOT, OR, TRUE


class BooleanTests(unittest.TestCase):
    def test_not_true_is_false(self):
        self.assertEqual(FALSE, NOT(TRUE))

    def test_not_false_is_true(self):
        self.assertEqual(TRUE, NOT(FALSE))

    def test_false_and_true_is_false(self):
        self.assertEqual(FALSE, AND(FALSE)(TRUE))

    def test_true_and_true_is_true(self):
        self.assertEqual(TRUE, AND(TRUE)(TRUE))

    def test_true_and_false_is_false(self):
        self.assertEqual(FALSE, AND(FALSE)(FALSE))

    def test_true_or_false_is_true(self):
        self.assertEqual(TRUE, OR(TRUE)(FALSE))

    def test_false_or_false_is_false(self):
        self.assertEqual(FALSE, OR(FALSE)(FALSE))

    def test_false_or_true_is_true(self):
        self.assertEqual(TRUE, OR(FALSE)(TRUE))


if __name__ == "__main__":
    unittest.main()
