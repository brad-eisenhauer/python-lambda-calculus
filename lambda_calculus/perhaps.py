class Perhaps:
    def __init__(self, value):
        self.value = value

    def __rshift__(self, other):
        if self.value is not None:
            return Perhaps(other(self.value))
        return self
