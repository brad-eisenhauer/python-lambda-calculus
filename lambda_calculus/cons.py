from lambda_calculus.boolean import FALSE, TRUE


def CONS(a):
    def inner(b):
        return lambda s: s(a)(b)

    return inner


def CAR(p):
    return p(TRUE)


def CDR(p):
    return p(FALSE)
