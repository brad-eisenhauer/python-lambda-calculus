from lambda_calculus.numbers import ISZERO, MUL, ONE, PRED


def Y(f):
    return (lambda x: f(lambda z: x(x)(z)))(lambda x: f(lambda z: x(x)(z)))


def R(f):
    return lambda n: ISZERO(n)(lambda: ONE)(lambda: MUL(n)(f(PRED(n))))()


FACT = Y(R)
