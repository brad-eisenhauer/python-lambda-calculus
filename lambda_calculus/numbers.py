from lambda_calculus.boolean import FALSE, TRUE
from lambda_calculus.cons import CAR, CDR, CONS


def ONE(f):
    return lambda x: f(x)


def TWO(f):
    return lambda x: f(f(x))


def THREE(f):
    return lambda x: f(f(f(x)))


def FOUR(f):
    return lambda x: f(f(f(f(x))))


def ZERO(f):
    return lambda x: x


def SUCC(n):
    return lambda f: lambda x: f(n(f)(x))


def ADD(a):
    def f(b):
        return b(SUCC)(a)

    return f


def MUL(a):
    return lambda b: lambda f: b(a(f))


def PRED(n):
    def T(p):
        return CONS(SUCC(CAR(p)))(CAR(p))

    return CDR(n(T)(CONS(ZERO)(ZERO)))


def SUB(a):
    return lambda b: b(PRED)(a)


def ISZERO(n):
    return n(lambda _: FALSE)(TRUE)
