# Python Lambda Calculus
Examples from David Beazley's [tutorial session](https://youtu.be/5C6sv7-eTKg) from PyCon 2019

[![Lambda Calculus: PyCon 2019](http://img.youtube.com/vi/5C6sv7-eTKg/0.jpg)](https://www.youtube.com/watch?v=5C6sv7-eTKg "Lambda Calculus: PyCon 2019")
